import Card from './card.js'

function newGame(container, cardsCount) {
  //создать поле
  let cardsNumberArray = []
  let cardsArray = []
  let firstCard = null
  let secondCard = null
  const restartButton = document.getElementById('restart-button');
  restartButton.classList.add('hidden');
  restartButton.addEventListener('click', () => {
    container.innerHTML = '';
    cardsNumberArray = [];
    cardsArray = [];
    firstCard = null;
    secondCard = null;
    newGame(container, cardsCount);
  });

  for (let i = 1; i <= cardsCount / 2; i++) {
    cardsNumberArray.push(i)
    cardsNumberArray.push(i)

  }
  cardsNumberArray = cardsNumberArray.sort(() => Math.random() - 0.5)

  for (const cardNumber of cardsNumberArray) {
    cardsArray.push(new Card(container, cardNumber, flip))
  }
  //логика
  function flip(card) {
    if (firstCard !== null && secondCard !== null) {
      if (firstCard.number != secondCard.number) {
        firstCard.open = false
        secondCard.open = false
        firstCard = null
        secondCard = null
      }
    }
    if (firstCard == null) {
      firstCard = card
    } else {
      if (secondCard = card) {
        secondCard = card
      }
    }
    if (firstCard !== null && secondCard !== null) {
      if (firstCard.number == secondCard.number) {
        firstCard.success = true
        secondCard.success = true
        firstCard = null
        secondCard = null
      }
    }
    if (document.querySelectorAll('.card.success').length == cardsNumberArray.length) {
      setTimeout("alert('Победа')", 200);
      restartButton.classList.remove('hidden');
    }
  }
}
newGame(document.getElementById('game'), 16)

